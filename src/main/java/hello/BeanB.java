package hello;

/**
 * Created by michal on 1/22/15.
 */
public class BeanB {

    private final BeanA beanA;

    public BeanB(BeanA beanA){
        this.beanA = beanA;
    }

    public BeanA getBeanA() {
        return beanA;
    }
}
