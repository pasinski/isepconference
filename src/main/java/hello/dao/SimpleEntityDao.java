package hello.dao;

import hello.entity.SimpleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by michal on 1/22/15.
 */

@Repository
public interface SimpleEntityDao extends JpaRepository<SimpleEntity, Long> {
}
