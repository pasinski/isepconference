package hello.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Created by michal on 1/22/15.
 */

@Repository
public class JustToTestEmAutowire {

    @Autowired
    public JustToTestEmAutowire(EntityManager em){
        this.em = em;
        System.out.println("*************** em injected   **********   " +  em);
    }

    private EntityManager em;
}
