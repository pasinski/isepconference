package hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by michal on 1/22/15.
 */

@Configuration
public class BeanAConfiguration {

    @Bean
    public BeanA getBeanA(){
        return new BeanA();
    }
}
