package hello.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by michal on 1/22/15.
 */
@Entity
public class SimpleEntity {
    private static final String GENERATOR = "generator";

    @Id
    @GenericGenerator(name = GENERATOR, strategy = "increment")
    @GeneratedValue(generator = GENERATOR)
    private Long id;

    private String properyOne;
    private String property2;

    public String getProperyOne() {
        return properyOne;
    }

    public void setProperyOne(String properyOne) {
        this.properyOne = properyOne;
    }

    public String getProperty2() {
        return property2;
    }

    public void setProperty2(String property2) {
        this.property2 = property2;
    }

    public Long getId() {
        return id;
    }
}
