package hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by michal on 1/22/15.
 */
@Configuration
public class BeanBConfiguration {

    @Bean
    public BeanB getConfiguredBeanB(BeanA beanA){
        return new BeanB(beanA);
    }

}
