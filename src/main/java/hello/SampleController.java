package hello;

import hello.entity.SimpleEntity;
import hello.service.SimpleEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Controller
@ComponentScan
@Configuration
@RestController
@EnableAutoConfiguration
public class SampleController implements CommandLineRunner {

    @Autowired FacebookLookupService facebookLookupService;

    @Autowired BeanB beanB;

    @Autowired
    SimpleEntityService simpleEntityService;

    @RequestMapping("/")
    String home() {
        return "Hello World!";
    }

    @RequestMapping("/facebook/{pageId}")
    Page facebookPage(@PathVariable("pageId") String pageId) throws InterruptedException, ExecutionException {
        Future<Page> page = facebookLookupService.findPage(pageId);
        return page.get();

    }

    @RequestMapping("/simpleEntity")
    SimpleEntity simpleEntity()  {
        SimpleEntity entity = new SimpleEntity();
        entity.setProperyOne("one");
        entity.setProperty2("two");
        SimpleEntity second = simpleEntityService.saveSimpleEntity(entity);
        SimpleEntity third = simpleEntityService.findById(second.getId());
        return third;
    }


    public static void main(String[] args) throws Exception {
       SpringApplication.run(SampleController.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(beanB.getBeanA());
    }
}