package hello.service;

import hello.dao.SimpleEntityDao;
import hello.entity.SimpleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

/**
 * Created by michal on 1/22/15.
 */

@Component
public class SimpleEntityService {

    private final SimpleEntityDao dao;

    @Autowired
    public SimpleEntityService(SimpleEntityDao dao){
        this.dao = dao;
    }

    @Transactional
    public SimpleEntity saveSimpleEntity(SimpleEntity entity){
        return dao.save(entity);
    }

    public SimpleEntity findById(Long id){
        return dao.findOne(id);
    }
}
